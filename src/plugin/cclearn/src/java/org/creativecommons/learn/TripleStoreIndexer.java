package org.creativecommons.learn;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.Text;
import org.apache.lucene.document.Field;
import org.apache.nutch.crawl.CrawlDatum;
import org.apache.nutch.crawl.Inlinks;
import org.apache.nutch.indexer.IndexingException;
import org.apache.nutch.indexer.IndexingFilter;
import org.apache.nutch.indexer.NutchDocument;
import org.apache.nutch.indexer.lucene.LuceneWriter;
import org.apache.nutch.parse.Parse;
import org.creativecommons.learn.oercloud.Feed;
import org.creativecommons.learn.oercloud.Resource;
import org.hsqldb.lib.Iterator;

import thewebsemantic.NotFoundException;

import com.hp.hpl.jena.graph.query.Element;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Literal;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.RDFNode;

public class TripleStoreIndexer implements IndexingFilter {

	protected Map<String, String> DEFAULT_NAMESPACES;

	public static final Log LOG = LogFactory.getLog(TripleStoreIndexer.class
			.getName());

	private Configuration conf;

	public TripleStoreIndexer() {
		LOG.info("Created TripleStoreIndexer.");

		// initialize the set of default mappings
		DEFAULT_NAMESPACES = new HashMap<String, String>();
		DEFAULT_NAMESPACES.put(CCLEARN.getURI(), CCLEARN.getDefaultPrefix());
		DEFAULT_NAMESPACES.put("http://purl.org/dc/elements/1.1/", "dct");
		DEFAULT_NAMESPACES.put("http://purl.org/dc/terms/", "dct");
		DEFAULT_NAMESPACES.put("http://www.w3.org/1999/02/22-rdf-syntax-ns#",
				"rdf");

	}


	@Override
	public void addIndexBackendOptions(Configuration conf) {
		// set up a set of fields to configure as lucene indexes (add FieldOptions)
		// 1. first add in DC terms for curator and feeds,
		// 2. then search configuration file for terms,
		// 3. then search thru triplestore for terms (that might arise from new metadata standards) 
		//    (encode the predicate terms into shorter terms with underscores)
		// 4. loop thru the collection (Hashset) and call LuceneWriter.addFieldOptions for each fieldname
		
		//1. fixed fieldnames
		Set<String> fieldNames = new HashSet<String>();
		
		// Define the curator and feed fields
		fieldNames.add(Search.CURATOR_INDEX_FIELD);
		fieldNames.add(Search.FEED_FIELD);
		
		// r2. read configuration file
		Configuration confFile = new Configuration();
		confFile.addResource("fieldname.xml");
		
		java.util.Iterator<Entry<String, String>> it = confFile.iterator();
		
		while (it.hasNext()) {
			 Entry<String, String> element = it.next();
			 fieldNames.add(element.getValue());
	
		}
		
		//3. read triplestore
		Model m;
		try {
			m = TripleStore.get().getModel();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			LOG.error("Unable to get model; " + e.toString());

			return;
		}

		// Create a new query
		String queryString = "SELECT ?s ?p ?o " + "WHERE {?s ?p ?o .}";

		Query query = QueryFactory.create(queryString);

		// Execute the query and obtain results
		QueryExecution qe = QueryExecutionFactory.create(query, m);
		ResultSet results = qe.execSelect();

		// Index the triples
		while (results.hasNext()) {
			QuerySolution stmt = results.nextSolution();
			RDFNode item = stmt.get("p");
			String colItem = collapseResource(item.toString() );
			fieldNames.add(colItem  );
		}

		// Important - free up resources used running the query
		qe.close();
		
		// 4. loop thru the collection
	   java.util.Iterator<String> colIt = fieldNames.iterator();
		while (colIt.hasNext()) {
			String element = colIt.next();
			LuceneWriter.addFieldOptions(element, LuceneWriter.STORE.YES,
					LuceneWriter.INDEX.UNTOKENIZED, confFile);
		}
	

	} // addIndexBackendOptions

	@Override
	public NutchDocument filter(NutchDocument doc, Parse parse, Text url,
			CrawlDatum datum, Inlinks inlinks) throws IndexingException {

		try {
			LOG.info("TripleStore: indexing " + url.toString());

			//insert some sample data twb
			Resource r = TripleStore.get().load(Resource.class, url.toString());
			
			Collection<String> Objects = r.getSubjects();
			Objects.add("books");
			Objects.add("courses");
			
	        TripleStore.get().save(r);
			
			
			// Index all triples
			LOG.debug("TripleStore: indexing all triples.");
			indexTriples(doc, url);

			// Follow special cases (curator)
			LOG.debug("TripleStore: indexing special cases.");
			this.indexSources(doc, TripleStore.get().loadDeep(Resource.class,
					url.toString()));

		} catch (NotFoundException e) {
			LOG.warn("Could not find " + url.toString()
					+ " in the Triple Store.");
			e.printStackTrace();
		} catch (Exception e) {
			LOG.error("An error occured while indexing " + url.toString());
			e.printStackTrace();
		}

		// Return the document
		return doc;

	} // public Document filter

	private void indexTriples(NutchDocument doc, Text url) {
		Model m;
		try {
			m = TripleStore.get().getModel();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			LOG.error("Unable to get model; " + e.toString());

			return;
		}

		// Create a new query
		String queryString = "SELECT ?p ?o " + "WHERE {" + "      <"
				+ url.toString() + "> ?p ?o ." + "      }";

		Query query = QueryFactory.create(queryString);

		// Execute the query and obtain results
		QueryExecution qe = QueryExecutionFactory.create(query, m);
		ResultSet results = qe.execSelect();

		// Index the triples
		while (results.hasNext()) {
			QuerySolution stmt = results.nextSolution();
			this.indexStatement(doc, stmt.get("p"), stmt.get("o"));
		}

		// Important - free up resources used running the query
		qe.close();
	}

	private void indexSources(NutchDocument doc, Resource resource) {

		for (Feed source : resource.getSources()) {

			// add the feed URL to the resource 
			doc.add(Search.FEED_FIELD, source.getUrl());

			// if this feed has curator information attached, index it as well
			String curator_url = "";
			if (source.getCurator() != null) {
				curator_url = source.getCurator().getUrl();
			}

			// LuceneWriter.add(doc, curator);
			doc.add(Search.CURATOR_INDEX_FIELD, curator_url);
		}
	}

	protected String collapseResource(String uri) {
		/*
		 * Given a Resource URI, collapse it using our default namespace
		 * mappings if possible. This is purely a convenience.
		 */

		for (String ns_url : DEFAULT_NAMESPACES.keySet()) {
			if (uri.startsWith(ns_url)) {
				return uri.replace(ns_url, "_" + DEFAULT_NAMESPACES.get(ns_url)
						+ "_");
			}
		}

		return uri;

	} // collapseResource

	private void indexStatement(NutchDocument doc, RDFNode pred_node,
			RDFNode obj_node) {
		
		Field.Index tokenized = Field.Index.NOT_ANALYZED;

		// index a single statement
		String predicate = pred_node.toString();
		String object = obj_node.toString();

		// read in URI->mnenomic configuration file, make URI2mnemonic
		//hash of the mappings
		Configuration conf = new Configuration();
		conf.addResource("fieldname.xml");	
				
		// see if we want to collapse the predicate into a shorter convenience
		// value
		if (pred_node.isResource()) {
			// check uri2 indexStatementemnemonic hash for the URI
			// if a value found for the URI, use that as lucene field name instead of hardcoded
			// terms (dct, rdf)
			if (conf.get(predicate) != null ) {
				predicate = conf.get(predicate);
			}
			else {
			// else do hard coded mapping
				predicate = collapseResource(pred_node.toString());
			}
		}
		// process the object...
		if (obj_node.isLiteral()) {
			object = ((Literal) obj_node).getValue().toString();
			tokenized = Field.Index.ANALYZED;
		}

		// add the field to the document
		Field statementField = new Field(predicate, object, Field.Store.YES,
				tokenized);

		LOG.debug("Adding (" + predicate + ", " + object + ").");

		//LuceneWriter.add(doc, statementField);

		doc.add(predicate, object);
	}

	public void setConf(Configuration conf) {
		this.conf = conf;
	}

	public Configuration getConf() {
		return this.conf;
	}
	
} // CuratorIndexer
